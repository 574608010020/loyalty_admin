<?php $page = 'reward_level'; include('template/header_temp.php'); include('template/menu_temp.php'); ?>
<link rel="stylesheet" type="text/css" href="assets/DataTables/jquery.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/css/dropzone.css"/>
<link rel="stylesheet" type="text/css" href="assets/css/dropzone.js"/>
<?php $item_id = $_COOKIE["reward_item_id"]; ?>
		<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop m-page__container m-body">
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">
									ข้อมูลของรางวัลสถานะ <?= $_COOKIE["level_title"]; ?>
								</h3>
							</div>
						</div>
					</div>
					<!-- END: Subheader -->
          <?php
						$GetRewardLevelById = new RewardLevel();
						$row = $GetRewardLevelById->fncGetRewardLevelById($item_id);
          ?>
          <div class="m-content loyalty-level">
            <div class="tab-pane active" id="m_user_profile_tab_1">
							<form action="curd/reward_level/insert.php" method="post" id="insert_form" enctype="multipart/form-data" class="m-form m-form--fit m-form--label-align-right" >
							  <div class="m-portlet__body">
							    <div class="form-group m-form__group row">
							      <label for="example-text-input" class="col-2 col-form-label">
							        Level
							      </label>
							      <div class="col-8">
							        <input class="form-control m-input" type="text" name="rewards_level_title" id="rewards_level_title" value="<?= $row['rewards_level_title']; ?>">
							      </div>
							    </div>
							    <div class="form-group m-form__group row">
							      <label for="example-text-input" class="col-2 col-form-label">
							        Point
							      </label>
							      <div class="col-8">
							        <input class="form-control m-input" type="number" name="rewards_level_point" id="rewards_level_point" value="<?= $row['rewards_level_point']; ?>">
							      </div>
							    </div>
									<div class="form-group m-form__group row">
							      <label for="example-text-input" class="col-2 col-form-label">
							        รูปภาพ
							      </label>
							      <div class="col-8">
											<input type="file" name="file[]" id="file" multiple class="form-control">
							        <!-- <input type="file" name="img[]" id="img" class="form-control" multiple> -->
							        <input type="text" name="file_path" id="file_path" value="<?= substr($row['rewards_level_image'],0,strrpos($row['rewards_level_image'], '/' )).'/'; ?>" class="form-control" />
							        <img src="http://localhost:8083/<?= $row['rewards_level_image']; ?>" name="rewards_level_image" id="rewards_level_image" class="img-fulid reward-item-img">
							      </div>
							    </div>
							  </div>
							  <div class="m-portlet__foot m-portlet__foot--fit">
							    <div class="m-form__actions">
							      <div class="row">
							        <div class="col-2"></div>
							        <div class="col-7">
							          <input type="hidden" name="item_id" id="item_id" value="<?= $row['id']; ?>"/>
		 									 <input type="submit" name="insert" id="insert" value="Update" class="btn btn-success" />
							        </div>
							      </div>
							    </div>
							  </div>
							</form>
            </div>
          </div>
        </div>
      </div>
      <!-- end::Body -->
<?php include("template/footer_temp.php"); ?>
<script>
// $(document).ready(function() {
// 	  $('#insert_form').on("submit", function(event){
// 	       event.preventDefault();
// 	       if($('#rewards_level_title').val() == "")
// 	       {
// 	            alert("rewards_level_title is required");
// 	       }
// 	       else if($('#rewards_level_point').val() == '')
// 	       {
// 	            alert("rewards_level_point is required");
// 	       }
// 	       else
// 	       {
// 	            $.ajax({
// 	                 url:"curd/reward_level/insert.php",
// 	                 method:"POST",
// 	                 data:$('#insert_form').serialize(),
// 	                 beforeSend:function(){
// 	                      $('#insert').val("Inserting");
// 	                 },
// 	                 success:function(data){
// 	                      $('#insert_form')[0].reset();
// 												$('#m_user_profile_tab_1').html(data);
// 												$('#m_user_profile_tab_1').val('');
// 	                 }
// 	            });
// 	       }
// 	  });
// });

function readURL(input) {
		if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
						$('#rewards_level_image').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
		}
}

$("#img").change(function(){
		readURL(this);
});
</script>
