<?php $page = 'users'; include('template/header_temp.php'); include('template/menu_temp.php'); ?>
<link rel="stylesheet" type="text/css" href="assets/DataTables/jquery.dataTables.min.css"/>
		<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop m-page__container m-body">
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">
								   ข้อมูลลูกค้า
								</h3>
							</div>
						</div>
					</div>
					<!-- END: Subheader -->
          <div class="m-content loyalty-level">
            <table id="users" class="table table-striped table-bordered text-center">
              <thead>
                  <tr>
                      <th width="5%">#</th>
                      <th width="20%">อีเมล</th>
                      <th width="15%">สถานะ</th>
                      <th width="20%">Lots</th>
                      <th width="20%">Point</th>
                  </tr>
              </thead>
              <tbody>
                  <?php
                    $GetUsers = new Users();
                    $objGetUsers = $GetUsers->fncUsersDetail();
                    while($row = mysqli_fetch_array($objGetUsers)){
                      switch ($row['id']) {
                        case 1:
                          $class = 'label-light-go';
                          break;
                        case 2:
                          $class = 'label-light-advance';
                          break;
                        case 3:
                          $class = 'label-light-expert';
                          break;
                        default:
                          $class = 'label-light-master';
                          break;
                      }
                  ?>
                    <tr>
                        <td></td>
                        <td><?= $row['email']; ?></td>
                        <td><span class="label label-inline <?= $class; ?>"><?= $row['level_title']; ?></span></td>
                        <td><?= $row['lot']; ?></td>
                        <td><?= $row['point']; ?></td>
                    </tr>
                  <? } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- end::Body -->
<?php include("template/footer_temp.php"); ?>
<script type="text/javascript" src="assets/DataTables/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    var table = $('#users').DataTable( {
        "order": [[ 0, 'desc' ]]
    } );

    table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
} );
</script>
