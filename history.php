<?php $page = 'history'; include('template/header_temp.php'); include('template/menu_temp.php'); ?>
<link rel="stylesheet" type="text/css" href="assets/DataTables/jquery.dataTables.min.css"/>
		<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop m-page__container m-body">
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">
									แจ้งรับของรางวัล
								</h3>
							</div>
						</div>
					</div>
					<!-- END: Subheader -->
          <div class="m-content loyalty-level">
            <table id="history" class="table table-striped table-bordered text-center">
              <thead>
                  <tr>
                      <th width="10%">วันที่แจ้งรับ</th>
                      <th width="15%">อีเมล</th>
                      <th width="10%">ของรางวัลสถานะ</th>
                      <th width="10%">Level</th>
                      <th width="20%">ของรางวัล</th>
                      <th width="10%">Point</th>
                      <th width="5%">จัดการ</th>
                  </tr>
              </thead>
              <tbody>
                  <?php
                    $GetHistory = new History();
                    $objGetHistory = $GetHistory->fncHistoryDetail();
                    while($row = mysqli_fetch_array($objGetHistory)){
                      switch ($row['id']) {
                        case 1:
                          $class = 'label-light-go';
                          break;
                        case 2:
                          $class = 'label-light-advance';
                          break;
                        case 3:
                          $class = 'label-light-expert';
                          break;
                        default:
                          $class = 'label-light-master';
                          break;
                      }
                  ?>
                    <tr>
                        <td><?= $row['timestamp']; ?></td>
                        <td><?= $row['email']; ?></td>
                        <td><span class="label label-inline <?= $class; ?>"><?= $row['level_title']; ?></span></td>
                        <td><?= $row['rewards_level_title']; ?></td>
                        <td><?= $row['reward_items_title']; ?></td>
                        <td><?= number_format($row['rewards_level_point']); ?></td>
                        <td><i class="fa-2x fa fa-pencil"></i> แก้ไข</td>
                    </tr>
                  <? } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- end::Body -->
<?php include("template/footer_temp.php"); ?>
<script type="text/javascript" src="assets/DataTables/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#history thead tr').clone(true).appendTo( '#history thead' );
    $('#history thead tr:eq(0) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" class="history-input-'+i+'"/ style="width:100%">' );

        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    var table = $('#history').DataTable( {
					language: {
					 searchPlaceholder: "อีเมล / ของรางวัล"
			 },
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": [6]
        }],
        "order": [[ 0, 'desc' ]]
    } );


    // table.on( 'order.dt search.dt', function () {
    //     table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
    //         cell.innerHTML = i+1;
    //     } );
    // } ).draw();
} );
</script>
