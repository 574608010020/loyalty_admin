<?php

include("conn.php");

/********** ข้อมูลสถานะ level **********/
class Level extends DB_con {
  function fncGetLevelDetail(){
    $sql = "SELECT * FROM level";
    $result =  mysqli_query( $this->dbcon,$sql);
    return $result;
  }

  function fncGetLevelById($item_id){
    $sql = "SELECT * FROM level WHERE id = $item_id";
    $result =  mysqli_query( $this->dbcon,$sql);
    return $result;
  }
}

/********** ข้อมูลการแจ้งรับของรางวัล **********/
class History extends DB_con {
  function fncHistoryDetail(){
    $sql = "SELECT b.email, e.id, e.level_title, d.rewards_level_title, f.reward_items_title, d.rewards_level_point, a.timestamp
            FROM history a
            LEFT JOIN users b ON a.user_id = b.id
            LEFT JOIN reward_items c ON a.reward_items_id = c.id
            LEFT JOIN rewards_level d ON c.reward_level_id = d.id
            LEFT JOIN level e ON d.level_id = e.id
            LEFT JOIN reward_items_detail_th f ON c.id = f.reward_items_id";
    $result =  mysqli_query( $this->dbcon,$sql);
    return $result;
  }
}

/********** ข้อมูลลูกค้า **********/
class Users extends DB_con {
  function fncUsersDetail(){
    $sql = "SELECT a.email, a.lot, a.point, b.level_title, b.id
            FROM users a
            LEFT JOIN level b ON a.level_id = b.id";
    $result =  mysqli_query( $this->dbcon,$sql);
    return $result;
  }
}

/********** ของรางวัลรวมสถานะ **********/
class RewardLevel extends DB_con {
  function fncGetRewardLevelDetail($level_id){
    $sql = "SELECT * FROM rewards_level
            WHERE level_id = $level_id";
    $result =  mysqli_query( $this->dbcon,$sql);
    return $result;
  }

  function fncGetRewardLevelById($item_id){
    $sql = "SELECT * FROM rewards_level
            WHERE id = $item_id";
    $result_chk = $this->dbcon->query($sql);
    $result = $result_chk->fetch_assoc();
    return $result;
  }

  function fncUpdateRewardLevelById($item_id,$rewards_level_title,$rewards_level_point){
    $sql = "UPDATE rewards_level
            SET rewards_level_title = '$rewards_level_title', rewards_level_point = '$rewards_level_point'
            WHERE id = $item_id";
    $result =  mysqli_query( $this->dbcon,$sql);
    return $result;
  }
}

/********** ข้อมูลของรางวัล **********/
class RewardItems extends DB_con {
  function fncGetRewardItems($level_id){
    $sql = "SELECT a.*, b.rewards_level_title
            FROM reward_items a
            LEFT JOIN rewards_level b ON a.reward_level_id = b.id
            LEFT JOIN level c ON b.level_id = c.id
            WHERE c.id = $level_id";
    $result =  mysqli_query( $this->dbcon,$sql);
    return $result;
  }

  function fncGetRewardItemsById($item_id){
    $sql = "SELECT a.*, b.rewards_level_title
            FROM reward_items a
            LEFT JOIN rewards_level b ON a.reward_level_id = b.id
            LEFT JOIN level c ON b.level_id = c.id
            WHERE a.id = $item_id";
    $result_chk = $this->dbcon->query($sql);
    $result = $result_chk->fetch_assoc();
    return $result;
  }
}

/********** รายละเอียดของรางวัล **********/
class ItemsDetail extends DB_con {
  function fncGetItemsDetail($get_lang){
    $sql = "SELECT d.level_title,c.rewards_level_title, a.*, b.reward_items_point
            FROM reward_items_detail_$get_lang a
            LEFT JOIN reward_items b ON a.reward_items_id = b.id
            LEFT JOIN rewards_level c ON b.reward_level_id = c.id
            LEFT JOIN level d ON c.level_id = d.id";
    $result =  mysqli_query( $this->dbcon,$sql);
    return $result;
  }

  function fncGetItemsById($item_id,$get_lang){
    $sql = "SELECT d.level_title, a.*, b.reward_items_point
            FROM reward_items_detail_$get_lang a
            LEFT JOIN reward_items b ON a.reward_items_id = b.id
            LEFT JOIN rewards_level c ON b.reward_level_id = c.id
            LEFT JOIN level d ON c.level_id = d.id
            WHERE a.id = $item_id";
    $result =  mysqli_query( $this->dbcon,$sql);
    return $result;
  }

  function fncUpdateItems($item_id,$get_lang,$reward_items_title,$reward_items_description){
    $sql = "UPDATE reward_items_detail_$get_lang
            SET reward_items_title = '$reward_items_title', reward_items_description = '$reward_items_description'
            WHERE id = $item_id";
    $result =  mysqli_query( $this->dbcon,$sql);
    return $result;
  }
}
