<?php $page = 'items_detail'; include('template/header_temp.php'); include('template/menu_temp.php'); ?>
<link rel="stylesheet" type="text/css" href="assets/DataTables/jquery.dataTables.min.css"/>
<?php $get_lang = $_COOKIE["get_lang"]; ?>
		<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop m-page__container m-body">
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">
									รายละเอียดของรางวัล
								</h3>
							</div>
						</div>
					</div>
					<!-- END: Subheader -->
          <div class="m-content loyalty-level" id="loyalty-level">
            <table id="reward_items" class="table table-striped table-bordered text-center">
              <thead>
                  <tr>
                      <th>#</th>
                      <th></th>
                      <th>สถานะ</th>
											<th>Level</th>
                      <th>ของรางวัล</th>
                      <th width="20%">รายละเอียด</th>
                      <th width="20%">จัดการ</th>
                  </tr>
              </thead>
              <tbody>
                  <?php
                    $GetItemsDetail = new ItemsDetail();
                    $objGetItemsDetail = $GetItemsDetail->fncGetItemsDetail($get_lang);
                    while($row = mysqli_fetch_array($objGetItemsDetail)){
                  ?>
                    <tr>
                        <td></td>
                        <td><?= $row['reward_items_point']; ?></td>
                        <td><?= $row['level_title']; ?></td>
												<td><?= $row['rewards_level_title']; ?></td>
                        <td><?= $row['reward_items_title']; ?></td>
                        <td class="description"><?= $row['reward_items_description']; ?></td>
                        <td>
													<button name="edit" id="<?= $row['id']; ?>" class="btn btn-edit edit_data" /><i class="fa-2x fa fa-pencil"></i> edit</button>
													<button name="view" id="<?= $row['id']; ?>" class="btn btn-view view_data" /><i class="fa-2x fa fa-eye"></i> view</button>
												</td>
                    </tr>
                  <? } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- end::Body -->

			<!-- start::modal -->
			<div id="dataModal" class="modal fade">
	      <div class="modal-dialog modal-dialog-centered modal-confirm-dialog modal-lg">
	           <div class="modal-content">
	                <div class="modal-header">
	                     <button type="button" class="close" data-dismiss="modal">&times;</button>
	                </div>
	                <div class="modal-body" id="item_detail">
	                </div>
	                <div class="modal-footer">
	                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                </div>
	           </div>
	      </div>
		 </div>

		 <div id="add_data_Modal" class="modal fade">
	     <div class="modal-dialog  modal-dialog-centered modal-confirm-dialog modal-lg">
	           <div class="modal-content">
	                <div class="modal-header">
											 <h4 class="modal-title">PHP Ajax Update MySQL Data Through Bootstrap Modal</h4>
	                     <button type="button" class="close" data-dismiss="modal">&times;</button>

	                </div>
	                <div class="modal-body">
	                     <form method="post" id="insert_form">
	                          <label>ของรางวัล</label>
	                          <input type="text" name="reward" id="reward" class="form-control" />
	                          <br />
	                          <label>รายละเอียด</label>
	                          <textarea name="detail" id="detail" class="form-control"></textarea>
	                          <br />
	                          <input type="hidden" name="item_id" id="item_id" />
	                          <input type="submit" name="insert" id="insert" value="Insert" class="btn btn-success" />
	                     </form>
	                </div>
	                <div class="modal-footer">
	                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                </div>
	           </div>
	      </div>
 			</div>
			 <!-- end::modal -->
<?php include("template/footer_temp.php"); ?>
<script type="text/javascript" src="assets/DataTables/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
		// start datatables //
    var table = $('#reward_items').DataTable( {
					language: {
					 searchPlaceholder: "สถานะ / ของรางวัล"
			 },
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": [0,1,5,6]
        },
        {
                "targets": [ 1 ],
                "visible": false,
                "searchable": false
        },],
        "order": [[ 1, 'asc' ]]
    } );

    table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
		// end datatables //

		// start modal //
		$('#add').click(function(){
           $('#insert').val("Insert");
           $('#insert_form')[0].reset();
      });
      $(document).on('click', '.edit_data', function(){
           var item_id = $(this).attr("id");
           $.ajax({
                url:"curd/item_detail/fetch.php",
                method:"POST",
                data:{item_id:item_id},
                dataType:"json",
                success:function(data){
                     $('#reward').val(data.reward_items_title);
                     $('#detail').val(data.reward_items_description);
                     $('#item_id').val(data.id);
                     $('#insert').val("Update");
                     $('#add_data_Modal').modal('show');
                }
           });
      });
      $('#insert_form').on("submit", function(event){
           event.preventDefault();
           if($('#reward').val() == "")
           {
                alert("Reward is required");
           }
           else if($('#detail').val() == '')
           {
                alert("Detail is required");
           }
           else
           {
                $.ajax({
                     url:"curd/item_detail/insert.php",
                     method:"POST",
                     data:$('#insert_form').serialize(),
                     beforeSend:function(){
                          $('#insert').val("Inserting");
                     },
                     success:function(data){
                          $('#insert_form')[0].reset();
                          $('#add_data_Modal').modal('hide');
                          $('#loyalty-level').html(data);
													$('#loyalty-level').val('');
                     }
                });
           }
      });
      $(document).on('click', '.view_data', function(){
           var item_id = $(this).attr("id");
           if(item_id != '')
           {
                $.ajax({
                     url:"curd/item_detail/select.php",
                     method:"POST",
                     data:{item_id:item_id},
                     success:function(data){
                          $('#item_detail').html(data);
                          $('#dataModal').modal('show');
                     }
                });
           }
      });
		// end modal //
} );
</script>
