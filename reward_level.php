<?php $page = 'reward_level'; include('template/header_temp.php'); include('template/menu_temp.php'); ?>
<link rel="stylesheet" type="text/css" href="assets/DataTables/jquery.dataTables.min.css"/>
<?php $level_id = $_COOKIE["level_id"]; ?>
		<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop m-page__container m-body">
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">
									ของรางวัลรวมสถานะ <?= $_COOKIE["level_title"]; ?>
								</h3>
							</div>
						</div>
					</div>
					<!-- END: Subheader -->
          <div class="m-content loyalty-level">
            <table id="reward_level" class="table table-striped table-bordered text-center">
              <thead>
                  <tr>
                      <th>#</th>
                      <th width="20%">ของรางวัล</th>
                      <th width="20%">Point</th>
                      <th>รูปภาพ</th>
                      <th width="20%">จัดการ</th>
                  </tr>
              </thead>
              <tbody>
                  <?php
                    $GetRewardLevelDetail = new RewardLevel();
                    $objGetRewardLevelDetail = $GetRewardLevelDetail->fncGetRewardLevelDetail($level_id);
                    while($row = mysqli_fetch_array($objGetRewardLevelDetail)){
                  ?>
                    <tr>
                        <td></td>
                        <td><?= $row['rewards_level_title']; ?></td>
                        <td><?= $row['rewards_level_point']; ?></td>
                        <td><img src="http://165.22.242.214/<?= $row['rewards_level_image']; ?>" class="reward-level-img"></td>
                        <td>
													<button name="edit" id="<?= $row['id']; ?>" class="btn btn-edit edit_data" onclick="EditRewardLevel('<?= "$row[id]" ?>')"/><i class="fa-2x fa fa-pencil"></i> edit</button>
													<button name="view" id="<?= $row['id']; ?>" class="btn btn-view view_data" /><i class="fa-2x fa fa-eye"></i> view</button>
												</td>
                    </tr>
                  <? } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- end::Body -->
			<!-- start::modal -->
			<div id="dataModal" class="modal fade">
	      <div class="modal-dialog modal-dialog-centered modal-confirm-dialog modal-lg">
	           <div class="modal-content">
	                <div class="modal-header">
	                     <button type="button" class="close" data-dismiss="modal">&times;</button>
	                </div>
	                <div class="modal-body" id="item_detail">
	                </div>
	                <div class="modal-footer">
	                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                </div>
	           </div>
	      </div>
		 </div>

		 <div id="add_data_Modal" class="modal fade">
	     <div class="modal-dialog  modal-dialog-centered modal-confirm-dialog modal-lg">
	           <div class="modal-content">
	                <div class="modal-header">
											 <h4 class="modal-title">PHP Ajax Update MySQL Data Through Bootstrap Modal</h4>
	                     <button type="button" class="close" data-dismiss="modal">&times;</button>

	                </div>
	                <div class="modal-body">
	                     <form method="post" id="insert_form">
	                          <label>ของรางวัล</label>
	                          <input type="text" name="rewards_level_title" id="rewards_level_title" class="form-control" />
	                          <br />
	                          <label>รายละเอียด</label>
	                          <input type="text" name="rewards_level_point" id="rewards_level_point" class="form-control">
	                          <br />
														<label>รูปภาพ</label>
	                           <input type="file" name="img" id="img" class="form-control" />
	                          <br />
														<div id="preview">
															<img src="" name="rewards_level_image" id="rewards_level_image" class="img-fulid reward-item-img">
														</div>
	                          <input type="hidden" name="item_id" id="item_id" />
	                          <input type="submit" name="insert" id="insert" value="Insert" class="btn btn-success" />
	                     </form>
	                </div>
	                <div class="modal-footer">
	                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                </div>
	           </div>
	      </div>
 			</div>
			<!-- end::modal -->
<?php include("template/footer_temp.php"); ?>
<script type="text/javascript" src="assets/DataTables/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    var t = $('#reward_level').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        },
				{
            "searchable": false,
            "orderable": false,
            "targets": 4
        },],
        "order": [[ 2, 'asc' ]]
    } );

    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

		// start modal //
		// $('#add').click(function(){
    //        $('#insert').val("Insert");
    //        $('#insert_form')[0].reset();
    //   });
      // $(document).on('click', '.edit_data', function(){
      //      var item_id = $(this).attr("id");
      //      $.ajax({
      //           url:"curd/reward_level/fetch.php",
      //           method:"POST",
      //           data:{item_id:item_id},
      //           dataType:"json",
      //           success:function(data){
      //                $('#rewards_level_title').val(data.rewards_level_title);
      //                $('#rewards_level_point').val(data.rewards_level_point);
			// 							 $('#rewards_level_image').attr('src','http://localhost:8083/'+data.rewards_level_image)
      //                $('#item_id').val(data.id);
      //                $('#insert').val("Update");
      //                $('#add_data_Modal').modal('show');
      //           }
      //      });
      // });
    //   $('#insert_form').on("submit", function(event){
    //        event.preventDefault();
    //        if($('#reward').val() == "")
    //        {
    //             alert("Reward is required");
    //        }
    //        else if($('#detail').val() == '')
    //        {
    //             alert("Detail is required");
    //        }
    //        else
    //        {
    //             $.ajax({
    //                  url:"curd/item_detail/insert.php",
    //                  method:"POST",
    //                  data:$('#insert_form').serialize(),
    //                  beforeSend:function(){
    //                       $('#insert').val("Inserting");
    //                  },
    //                  success:function(data){
    //                       $('#insert_form')[0].reset();
    //                       $('#add_data_Modal').modal('hide');
    //                       $('#loyalty-level').html(data);
		// 											$('#loyalty-level').val('');
    //                  }
    //             });
    //        }
    //   });
      $(document).on('click', '.view_data', function(){
           var item_id = $(this).attr("id");
           if(item_id != '')
           {
                $.ajax({
                     url:"curd/reward_level/select.php",
                     method:"POST",
                     data:{item_id:item_id},
                     success:function(data){
                          $('#item_detail').html(data);
                          $('#dataModal').modal('show');
                     }
                });
           }
      });
		// end modal //
} );

function EditRewardLevel(reward_item_id){
	document.cookie = "reward_item_id="+ reward_item_id;
	window.location = 'reward_level_edit';
}
</script>
