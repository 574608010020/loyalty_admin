<?php $page = 'reward_item'; include('template/header_temp.php'); include('template/menu_temp.php'); ?>
<link rel="stylesheet" type="text/css" href="assets/DataTables/jquery.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/css/dropzone.css"/>
<link rel="stylesheet" type="text/css" href="assets/css/dropzone.js"/>
<?php $item_id = $_COOKIE["reward_item_id"]; ?>
		<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop m-page__container m-body">
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">
									ข้อมูลของรางวัลสถานะ <?= $_COOKIE["level_title"]; ?>
								</h3>
							</div>
						</div>
					</div>
					<!-- END: Subheader -->
          <?php
            $GetRewardItemsById = new RewardItems();
            $row = $GetRewardItemsById->fncGetRewardItemsById($item_id);
          ?>
          <div class="m-content loyalty-level">
            <div class="tab-pane active" id="m_user_profile_tab_1">
							<form action="#" enctype="multipart/form-data" class="m-form m-form--fit m-form--label-align-right" >
                <div class="m-portlet__body">
                  <div class="form-group m-form__group row">
                    <label for="example-text-input" class="col-2 col-form-label">
                      สถานะ
                    </label>
                    <div class="col-8">
                      <input class="form-control m-input" type="text" value="<?= $row['rewards_level_title']; ?>" disabled>
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label for="example-text-input" class="col-2 col-form-label">
                      Point
                    </label>
                    <div class="col-8">
                      <input class="form-control m-input" type="number" value="<?= $row['reward_items_point']; ?>">
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label for="example-text-input" class="col-2 col-form-label">
                      รูปภาพ
                    </label>
                    <div class="col-8">
                      <?php
                        $img = explode("|", $row['reward_items_img']);
												$file_path = substr($img[0],0,strrpos($img[0], '/' )).'/';
												$file_img = '';
                        foreach($img as $data_img){
                          echo "<img src='http://localhost:8083/$data_img' class='img-fulid reward-item-img'>";
													$file_img .= $data_img.'|';
                        }
												//echo $file_img;
                      ?>
                      <div id="preview"></div>
                    </div>
                  </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions">
                    <div class="row">
                      <div class="col-2"></div>
                      <div class="col-7">
                        <button type="reset" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                          Save changes
                        </button>
                        &nbsp;&nbsp;
                        <button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                          Cancel
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- end::Body -->
<?php include("template/footer_temp.php"); ?>
<script>
function previewImages() {

  var $preview = $('#preview').empty();
  if (this.files) $.each(this.files, readAndPreview);

  function readAndPreview(i, file) {

    if (!/\.(jpe?g|png|gif)$/i.test(file.name)){
      return alert(file.name +" is not an image");
    } // else...

    var reader = new FileReader();

    $(reader).on("load", function() {
      $preview.append($("<img/>", {src:this.result, height:100}));
    });

    reader.readAsDataURL(file);

  }

}

var btns=document.getElementsByTagName('input');
for(i=0;i<btns.length;i++)
{
    if(btns[i].type==='button' && btns[i].className==='remove')
    btns[i].onclick=remove;
}
function remove(event){
  var e = event || window.event;
  var el = e.target || e.srcElement;
  document.animalForm.removeChild(el.parentNode);
}
</script>
