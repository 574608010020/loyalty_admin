<?php
include("../../func/function.php");
if(!empty($_POST))
{
    $output = '';
    $message = '';
    $item_id = $_POST["item_id"];
    $rewards_level_title = $_POST["rewards_level_title"];
    $rewards_level_point = $_POST["rewards_level_point"];
    $file_path = $_POST["file_path"];
    $file_name = $_FILES['file'];
    if($_POST["item_id"] != '')
    {
        $countfiles = count($_FILES['file']['name']);
        // Looping all files
       for($i=0;$i<$countfiles;$i++){
        $filename = $_FILES['file']['name'][$i];

        // Upload file
        move_uploaded_file($_FILES['file']['tmp_name'][$i],'images/'.$filename);

       }
        $UpdateRewardLevelById = new RewardLevel();
        $objUpdateRewardLevelById = $UpdateRewardLevelById->fncUpdateRewardLevelById($item_id,$rewards_level_title,$rewards_level_point);
        $message = 'Data Updated';
    }
    else
    {
         $query = "
         INSERT INTO tbl_employee(name, address, gender, designation, age)
         VALUES('$name', '$address', '$gender', '$designation', '$age');
         ";
         $message = 'Data Inserted';
    }
    if($objUpdateRewardLevelById)
    {
         $output .= '<label class="text-success">' . $message . '</label>';
         $GetRewardLevelById = new RewardLevel();
         $row = $GetRewardLevelById->fncGetRewardLevelById($item_id);
         $output .= '
         <form method="post" id="insert_form" enctype="multipart/form-data" class="m-form m-form--fit m-form--label-align-right" >
           <div class="m-portlet__body">
             <div class="form-group m-form__group row">
               <label for="example-text-input" class="col-2 col-form-label">
                 Level
               </label>
               <div class="col-8">
                 <input class="form-control m-input" type="text" name="rewards_level_title" id="rewards_level_title" value="'.$row["rewards_level_title"].'">
               </div>
             </div>
             <div class="form-group m-form__group row">
               <label for="example-text-input" class="col-2 col-form-label">
                 Point
               </label>
               <div class="col-8">
                 <input class="form-control m-input" type="number" name="rewards_level_point" id="rewards_level_point" value="'.$row["rewards_level_point"].'">
               </div>
             </div>
             <div class="form-group m-form__group row">
               <label for="example-text-input" class="col-2 col-form-label">
                 รูปภาพ
               </label>
               <div class="col-8">
                 <input type="file" name="img" id="img" class="form-control">
                 <input type="hidden" name="file_path" id="file_path" value="'.substr($row['rewards_level_image'],0,strrpos($row['rewards_level_image'], '/' )).'/'.'" class="form-control" />
                 <img src="http://localhost:8083/'.$row['rewards_level_image'].'" name="rewards_level_image" id="rewards_level_image" class="img-fulid reward-item-img">
               </div>
             </div>
           </div>
           <div class="m-portlet__foot m-portlet__foot--fit">
             <div class="m-form__actions">
               <div class="row">
                 <div class="col-2"></div>
                 <div class="col-7">
                   <input type="hidden" name="item_id" id="item_id" value="'.$row['id'].'"/>
                  <input type="submit" name="insert" id="insert" value="Update" class="btn btn-success" />
                 </div>
               </div>
             </div>
           </div>
         </form>
         ';
    }
    echo $output;
}?>
<script>
$(document).ready(function() {
	  $('#insert_form').on("submit", function(event){
	       event.preventDefault();
	       if($('#rewards_level_title').val() == "")
	       {
	            alert("rewards_level_title is required");
	       }
	       else if($('#rewards_level_point').val() == '')
	       {
	            alert("rewards_level_point is required");
	       }
	       else
	       {
	            $.ajax({
	                 url:"curd/reward_level/insert.php",
	                 method:"POST",
	                 data:$('#insert_form').serialize(),
	                 beforeSend:function(){
	                      $('#insert').val("Inserting");
	                 },
	                 success:function(data){
	                      $('#insert_form')[0].reset();
												$('#m_user_profile_tab_1').html(data);
												$('#m_user_profile_tab_1').val('');
	                 }
	            });
	       }
	  });
});

function readURL(input) {
		if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
						$('#rewards_level_image').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
		}
}

$("#img").change(function(){
		readURL(this);
});
</script>
