<?php
include("../../func/function.php");
$get_lang = $_COOKIE["get_lang"];
if(!empty($_POST))
{
    $output = '';
    $message = '';
    $item_id = $_POST["item_id"];
    $reward_items_title = $_POST["reward"];
    $reward_items_description = $_POST["detail"];
    if($_POST["item_id"] != '')
    {
         $UpdateItems = new ItemsDetail();
         $objUpdateItems = $UpdateItems->fncUpdateItems($item_id,$get_lang,$reward_items_title,$reward_items_description);
         $message = 'Data Updated';
    }
    else
    {
         $query = "
         INSERT INTO tbl_employee(name, address, gender, designation, age)
         VALUES('$name', '$address', '$gender', '$designation', '$age');
         ";
         $message = 'Data Inserted';
    }
    if($objUpdateItems)
    {
         $output .= '<label class="text-success">' . $message . '</label>';
         $output .= '
                     <table id="reward_items" class="table table-striped table-bordered text-center">
                       <thead>
                           <tr>
                               <th>#</th>
                               <th></th>
                               <th>สถานะ</th>
                               <th>Level</th>
                               <th>ของรางวัล</th>
                               <th width="20%">รายละเอียด</th>
                               <th width="20%">จัดการ</th>
                           </tr>
                       </thead>
                       <tbody>
         ';
         $GetItemsDetail = new ItemsDetail();
         $objGetItemsDetail = $GetItemsDetail->fncGetItemsDetail($get_lang);
         while($row = mysqli_fetch_array($objGetItemsDetail)){
              $output .= '
                   <tr>
                       <td></td>
                       <td>'.$row["reward_items_point"].'</td>
                       <td>'.$row["level_title"].'</td>
                       <td>'.$row['rewards_level_title'].'</td>
                       <td>'. $row["reward_items_title"].'</td>
                       <td class="description">'.$row["reward_items_description"].'</td>
                       <td>
                         <button name="edit" id="'.$row["id"].'" class="btn btn-edit edit_data"><i class="fa-2x fa fa-pencil"></i> edit</button>
                         <button name="view" id="'. $row["id"].'" class="btn btn-view view_data"><i class="fa-2x fa fa-eye"></i> view</button>
                       </td>
                   </tr>
              ';
         }
         $output .= '</tbody>
                     </table>';
    }
    echo $output;
}?>
<script>
$(document).ready(function() {
		// start datatables //
    var table = $('#reward_items').DataTable( {
					language: {
					 searchPlaceholder: "สถานะ / ของรางวัล"
			 },
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": [0,1,5,6]
        },
        {
                "targets": [ 1 ],
                "visible": false,
                "searchable": false
        },],
        "order": [[ 1, 'asc' ]]
    } );

    table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
		// end datatables //
  } );
</script>
