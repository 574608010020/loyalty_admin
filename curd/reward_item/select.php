<?php
 include("../../func/function.php");
 if(isset($_POST["item_id"]))
 {
      $output = '';
      $item_id = $_POST["item_id"];
      $GetRewardItemsById = new RewardItems();
      $row = $GetRewardItemsById->fncGetRewardItemsById($item_id);
      $output .= '
      <div class="table-responsive">
           <table class="table table-bordered">';
           $data_img = explode("|", $row['reward_items_img']);
           $output .= '
                <tr>
                     <td><label>สถานะ</label></td>
                     <td>'.$row['rewards_level_title'].'</td>
                </tr>
                <tr>
                     <td><label>Point</label></td>
                     <td>'.number_format($row['reward_items_point']).'</td>
                </tr>
                <tr>
                     <td><label>รูปภาพ</label></td>
                     <td>';
                       foreach($data_img as $img){
            $output .=  "<img src='http://165.22.242.214/".$img."' class='img-fulid reward-item-img'>";
                       }
            $output .= '
                     </td>
                </tr>
           ';
      $output .= '
           </table>
      </div>
      ';
      echo $output;
 }
 ?>
