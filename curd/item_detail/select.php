<?php
 include("../../func/function.php");
 $get_lang = $_COOKIE["get_lang"];
 if(isset($_POST["item_id"]))
 {
      $output = '';
      $item_id = $_POST["item_id"];
      $GetItemsById = new ItemsDetail();
      $objGetItemsById = $GetItemsById->fncGetItemsById($item_id,$get_lang);
      $output .= '
      <div class="table-responsive">
           <table class="table table-bordered">';
      while($row = mysqli_fetch_array($objGetItemsById))
      {
           $output .= '
                <tr>
                     <td width="30%"><label>สถานะ</label></td>
                     <td width="70%">'.$row["level_title"].'</td>
                </tr>
                <tr>
                     <td width="30%"><label>Point</label></td>
                     <td width="70%">'.$row["reward_items_point"].'</td>
                </tr>
                <tr>
                     <td width="30%"><label>ของรางวัล</label></td>
                     <td width="70%">'.$row["reward_items_title"].'</td>
                </tr>
                <tr>
                     <td width="30%"><label>รายละเอียด</label></td>
                     <td width="70%">'.$row["reward_items_description"].'</td>
                </tr>
           ';
      }
      $output .= '
           </table>
      </div>
      ';
      echo $output;
 }
 ?>
