<?php $page = 'level'; include('template/header_temp.php'); include('template/menu_temp.php'); ?>
		<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop m-page__container m-body">
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">
									ข้อมูลสถานะ (Level)
								</h3>
							</div>
						</div>
					</div>
					<!-- END: Subheader -->
					<div class="m-content loyalty-level">
						<table id="level" class="table text-center">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col" width="20%">ชื่อสถานะ</th>
									<th scope="col">รูปภาพสถานะ</th>
									<th scope="col">รูปภาพของรางวัลสถานะ</th>
									<th scope="col" width="20%">จัดการข้อมูล</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$GetLevelDetail = new Level();
									$objGetLevelDetail = $GetLevelDetail->fncGetLevelDetail();
									while($row = mysqli_fetch_array($objGetLevelDetail)){
								?>
									<tr>
										<td><?= $row['id']; ?></td>
										<td><?= $row['level_title']; ?></td>
										<td><img src="http://165.22.242.214/<?= $row['level_image']; ?>" class="level-img"></td>
										<td><img src="http://165.22.242.214/<?= $row['level_reward']; ?>" class="level-reward-img"></td>
										<td>
											<button name="edit" id="<?= $row['id']; ?>" class="btn btn-edit edit_data" /><i class="fa-2x fa fa-pencil"></i> edit</button>
											<button name="view" id="<?= $row['id']; ?>" class="btn btn-view view_data" /><i class="fa-2x fa fa-eye"></i> view</button>
										</td>
									</tr>
								<? } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- end::Body -->
			<!-- start::modal -->
			<div id="dataModal" class="modal fade">
	      <div class="modal-dialog modal-dialog-centered modal-confirm-dialog modal-lg">
	           <div class="modal-content">
	                <div class="modal-header">
	                     <button type="button" class="close" data-dismiss="modal">&times;</button>
	                </div>
	                <div class="modal-body" id="item_detail">
	                </div>
	                <div class="modal-footer">
	                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                </div>
	           </div>
	      </div>
		 </div>

		 <div id="add_data_Modal" class="modal fade">
	     <div class="modal-dialog  modal-dialog-centered modal-confirm-dialog modal-lg">
	           <div class="modal-content">
	                <div class="modal-header">
											 <h4 class="modal-title">PHP Ajax Update MySQL Data Through Bootstrap Modal</h4>
	                     <button type="button" class="close" data-dismiss="modal">&times;</button>

	                </div>
	                <div class="modal-body">
	                     <form method="post" id="insert_form">
	                          <label>ของรางวัล</label>
	                          <input type="text" name="reward" id="reward" class="form-control" />
	                          <br />
	                          <label>รายละเอียด</label>
	                          <textarea name="detail" id="detail" class="form-control"></textarea>
	                          <br />
	                          <input type="hidden" name="item_id" id="item_id" />
	                          <input type="submit" name="insert" id="insert" value="Insert" class="btn btn-success" />
	                     </form>
	                </div>
	                <div class="modal-footer">
	                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                </div>
	           </div>
	      </div>
 			</div>
			<!-- end::modal -->
<?php include("template/footer_temp.php"); ?>
<script>
$(document).ready(function() {
// start modal //
// $('#add').click(function(){
//        $('#insert').val("Insert");
//        $('#insert_form')[0].reset();
//   });
//   $(document).on('click', '.edit_data', function(){
//        var item_id = $(this).attr("id");
//        $.ajax({
//             url:"curd/item_detail/fetch.php",
//             method:"POST",
//             data:{item_id:item_id},
//             dataType:"json",
//             success:function(data){
//                  $('#reward').val(data.reward_items_title);
//                  $('#detail').val(data.reward_items_description);
//                  $('#item_id').val(data.id);
//                  $('#insert').val("Update");
//                  $('#add_data_Modal').modal('show');
//             }
//        });
//   });
//   $('#insert_form').on("submit", function(event){
//        event.preventDefault();
//        if($('#reward').val() == "")
//        {
//             alert("Reward is required");
//        }
//        else if($('#detail').val() == '')
//        {
//             alert("Detail is required");
//        }
//        else
//        {
//             $.ajax({
//                  url:"curd/item_detail/insert.php",
//                  method:"POST",
//                  data:$('#insert_form').serialize(),
//                  beforeSend:function(){
//                       $('#insert').val("Inserting");
//                  },
//                  success:function(data){
//                       $('#insert_form')[0].reset();
//                       $('#add_data_Modal').modal('hide');
//                       $('#loyalty-level').html(data);
// 											$('#loyalty-level').val('');
//                  }
//             });
//        }
//   });
	$(document).on('click', '.view_data', function(){
			 var item_id = $(this).attr("id");
			 if(item_id != '')
			 {
						$.ajax({
								 url:"curd/level/select.php",
								 method:"POST",
								 data:{item_id:item_id},
								 success:function(data){
											$('#item_detail').html(data);
											$('#dataModal').modal('show');
								 }
						});
			 }
	});
// end modal //
} );
</script>
