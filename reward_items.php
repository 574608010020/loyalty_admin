<?php $page = 'reward_item'; include('template/header_temp.php'); include('template/menu_temp.php'); ?>
<link rel="stylesheet" type="text/css" href="assets/DataTables/jquery.dataTables.min.css"/>
<?php
	$level_id = $_COOKIE["level_id"];
?>
		<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop m-page__container m-body">
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">
									ข้อมูลของรางวัลสถานะ <?= $_COOKIE["level_title"]; ?>
								</h3>
							</div>
						</div>
					</div>
					<!-- END: Subheader -->
          <div class="m-content loyalty-level">
            <table id="reward_items" class="table table-striped table-bordered text-center">
              <thead>
                  <tr>
                      <th>#</th>
                      <th width="20%">ของรางวัล</th>
                      <th width="20%">Point</th>
                      <th>รูปภาพ</th>
                      <th width="20%">จัดการ</th>
                  </tr>
              </thead>
              <tbody>
                  <?php
                    $GetRewardItems = new RewardItems();
                    $objGetRewardItems = $GetRewardItems->fncGetRewardItems($level_id);
                    while($row = mysqli_fetch_array($objGetRewardItems)){
											$data_img = explode("|", $row['reward_items_img'])[0];
                  ?>
                    <tr>
                        <td></td>
                        <td><?= $row['rewards_level_title']; ?></td>
                        <td><?= number_format($row['reward_items_point']); ?></td>
                        <td><img src="http://165.22.242.214/<?= $data_img; ?>" class="img-fulid reward-item-img"></td>
                        <td><button name="edit" id="<?= $row['id']; ?>" class="btn btn-edit edit_data" onclick="EditRewardItem('<?= "$row[id]" ?>')" /><i class="fa-2x fa fa-pencil"></i> edit</button>
														<button name="view" id="<?= $row['id']; ?>" class="btn btn-view view_data" /><i class="fa-2x fa fa-eye"></i> view</button></td>
                    </tr>
                  <? } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- end::Body -->

			<!-- start::modal -->
			<div id="dataModal" class="modal fade">
	      <div class="modal-dialog modal-dialog-centered modal-confirm-dialog modal-lg">
	           <div class="modal-content">
	                <div class="modal-header">
	                     <button type="button" class="close" data-dismiss="modal">&times;</button>
	                </div>
	                <div class="modal-body" id="item_detail">
	                </div>
	                <div class="modal-footer">
	                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                </div>
	           </div>
	      </div>
		 </div>
			<!-- end::modal -->
<?php include("template/footer_temp.php"); ?>
<script type="text/javascript" src="assets/DataTables/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    var t = $('#reward_items').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        },
        {
            "searchable": false,
            "orderable": false,
            "targets": 4
        },],
        "order": [[ 2, 'asc' ]]
    } );

    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

		// start modal //
		$('#add').click(function(){
           $('#insert').val("Insert");
           $('#insert_form')[0].reset();
      });
      // $(document).on('click', '.edit_data', function(){
      //      var item_id = $(this).attr("id");
      //      $.ajax({
      //           url:"curd/reward_item/fetch.php",
      //           method:"POST",
      //           data:{item_id:item_id},
      //           dataType:"json",
      //           success:function(data){
      //                $('#rewards_level_title').val(data.rewards_level_title);
      //                $('#reward_items_point').val(data.reward_items_point);
			// 							 // $('#reward_items_img').text(data.reward_items_img);
      //                $('#item_id').val(data.id);
      //                $('#insert').val("Update");
			// 							 document.cookie = "reward_items_img="+ data.reward_items_img;
      //                $('#add_data_Modal').modal('show');
      //           }
      //      });
      // });
      $('#insert_form').on("submit", function(event){
           event.preventDefault();
           if($('#reward').val() == "")
           {
                alert("Reward is required");
           }
           else if($('#detail').val() == '')
           {
                alert("Detail is required");
           }
           else
           {
                $.ajax({
                     url:"curd/item_detail/insert.php",
                     method:"POST",
                     data:$('#insert_form').serialize(),
                     beforeSend:function(){
                          $('#insert').val("Inserting");
                     },
                     success:function(data){
                          $('#insert_form')[0].reset();
                          $('#add_data_Modal').modal('hide');
                          $('#loyalty-level').html(data);
													$('#loyalty-level').val('');
                     }
                });
           }
      });
      $(document).on('click', '.view_data', function(){
           var item_id = $(this).attr("id");
           if(item_id != '')
           {
                $.ajax({
                     url:"curd/reward_item/select.php",
                     method:"POST",
                     data:{item_id:item_id},
                     success:function(data){
                          $('#item_detail').html(data);
                          $('#dataModal').modal('show');
                     }
                });
           }
      });
		// end modal //
});

		function EditRewardItem(reward_item_id){
			document.cookie = "reward_item_id="+ reward_item_id;
			window.location = 'reward_item_edit';
		}
</script>
