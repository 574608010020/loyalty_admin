<!-- begin::Footer -->
			<footer class="m-grid__item m-footer ">
				<div class="m-container m-container--fluid m-container--full-height m-page__container">
					<div class="m-footer__wrapper">
						<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
							<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
								<span class="m-footer__copyright">
									2021 &copy; GOFX Thailand Co., Ltd.
								</span>
							</div>
						</div>
					</div>
				</div>
			</footer>
			<!-- end::Footer -->
		</div>
		<!-- end:: Page -->

	    <!-- begin::Scroll Top -->
		<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
			<i class="la la-arrow-up"></i>
		</div>
		<!-- end::Scroll Top -->

    <!--begin::Base Scripts -->
    <script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
    <script src="assets/css/scripts.bundle.js" type="text/javascript"></script>
    <!--end::Base Scripts -->

    <!--begin::Page Vendors -->
    <script src="../assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
    <!--end::Page Vendors -->

    <!--begin::Page Snippets -->
    <script src="../assets/app/js/dashboard.js" type="text/javascript"></script>
    <!--end::Page Snippets -->

    <!-- begin::Page Loader -->
    <script>
          $(window).on('load', function() {
              $('body').removeClass('m-page--loading');
          });
    </script>
    <!-- end::Page Loader -->

	</body>
	<!-- end::Body -->
</html>
