<div class="m-header__bottom">
  <div class="m-container m-container--fluid m-container--full-height m-page__container">
    <div class="m-stack m-stack--ver m-stack--desktop">
      <!-- begin::Horizontal Menu -->
      <div class="m-stack__item m-stack__item--fluid m-header-menu-wrapper">
        <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-light " id="m_aside_header_menu_mobile_close_btn">
          <i class="la la-close"></i>
        </button>
        <div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-dark m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-light m-aside-header-menu-mobile--submenu-skin-light "  >
          <ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
            <li class="m-menu__item  m-menu__item--submenu m-menu__item--tabs <?php if($page == 'index' || $page == 'history' || $page == 'users'){echo "m-menu__item--active  m-menu__item--active-tab"; } ?>"  data-menu-submenu-toggle="tab" aria-haspopup="true">
              <a  href="index" class="m-menu__link m-menu__toggle">
                <span class="m-menu__link-text">
                  Dashboard
                </span>
                <i class="m-menu__hor-arrow la la-angle-down"></i>
                <i class="m-menu__ver-arrow la la-angle-right"></i>
              </a>
              <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left m-menu__submenu--tabs">
                <span class="m-menu__arrow m-menu__arrow--adjust"></span>
                <ul class="m-menu__subnav">
                  <li class="m-menu__item "  data-redirect="true" aria-haspopup="true">
                    <a  href="index" class="m-menu__link <?php if($page == 'index'){echo "menu__active"; } ?>">
                      <i class="m-menu__link-icon la la-home"></i>
                      <span class="m-menu__link-text">
                        หน้าแรก
                      </span>
                    </a>
                  </li>
                  <li class="m-menu__item "  aria-haspopup="true">
                    <a  href="history" class="m-menu__link <?php if($page == 'history'){echo "menu__active"; } ?>">
                      <i class="m-menu__link-icon la la-history"></i>
                      <span class="m-menu__link-text">
                        ข้อมูลการแจ้งรับของรางวัล
                      </span>
                    </a>
                  </li>
                  <li class="m-menu__item "  aria-haspopup="true">
                    <a  href="users" class="m-menu__link <?php if($page == 'users'){echo "menu__active"; } ?>">
                      <i class="m-menu__link-icon la la-user"></i>
                      <span class="m-menu__link-text">
                        ข้อมูลลูกค้า
                      </span>
                    </a>
                  </li>
                </ul>
              </div>
            </li>
            <li class="m-menu__item  m-menu__item--submenu m-menu__item--tabs <?php if($page == 'level' || $page == 'reward_level' || $page == 'reward_item' || $page == 'items_detail'){echo "m-menu__item--active  m-menu__item--active-tab"; } ?>"  data-menu-submenu-toggle="tab" aria-haspopup="true">
              <a  href="#" class="m-menu__link m-menu__toggle">
                <span class="m-menu__link-text">
                  จัดการข้อมูล
                </span>
                <i class="m-menu__hor-arrow la la-angle-down"></i>
                <i class="m-menu__ver-arrow la la-angle-right"></i>
              </a>
              <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left m-menu__submenu--tabs">
                <span class="m-menu__arrow m-menu__arrow--adjust"></span>
                <ul class="m-menu__subnav">
                  <li class="m-menu__item "  data-redirect="true" aria-haspopup="true">
                    <a  href="level" class="m-menu__link <?php if($page == 'level'){echo "menu__active"; } ?>">
                      <i class="m-menu__link-icon la la-trophy"></i>
                      <span class="m-menu__link-text">
                        ข้อมูลสถานะ (Level)
                      </span>
                    </a>
                  </li>
                  <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel m-menu__item--submenu-tabs"  data-menu-submenu-toggle="click" data-redirect="true" aria-haspopup="true">
                    <a  href="#" class="m-menu__link m-menu__toggle <?php if($page == 'reward_level'){echo "menu__active"; } ?>">
                      <i class="m-menu__link-icon la la-gift"></i>
                      <span class="m-menu__link-text">
                        ของรางวัลรวมแต่ละสถานะ
                      </span>
                      <i class="m-menu__hor-arrow la la-angle-down"></i>
                      <i class="m-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="m-menu__submenu  m-menu__submenu--fixed m-menu__submenu--left" style="width:300px">
                      <span class="m-menu__arrow m-menu__arrow--adjust"></span>
                      <div class="m-menu__subnav">
                        <ul class="m-menu__content">
                          <li class="m-menu__item">
                            <h3 class="m-menu__heading m-menu__toggle">
                              <span class="m-menu__link-text">
                                สถานะ
                              </span>
                              <i class="m-menu__ver-arrow la la-angle-right"></i>
                            </h3>
                            <ul class="m-menu__inner">
                              <li class="m-menu__item "  data-redirect="true" aria-haspopup="true">
                                <a  href="reward_level" class="m-menu__link <?php if($page == 'reward_level' && $_COOKIE["level_title"] == 'Go Trader' ){echo "menu__active"; } ?>" onclick="RewardLevel('Go Trader','1')">
                                  <i class="m-menu__link-bullet m-menu__link-bullet--line">
                                    <span></span>
                                  </i>
                                  <span class="m-menu__link-text">
                                    Go Trader
                                  </span>
                                </a>
                              </li>
                              <li class="m-menu__item "  data-redirect="true" aria-haspopup="true">
                                <a  href="reward_level" class="m-menu__link <?php if($page == 'reward_level' && $_COOKIE["level_title"] == 'Advance Trader' ){echo "menu__active"; } ?>" onclick="RewardLevel('Advance Trader','2')">
                                  <i class="m-menu__link-bullet m-menu__link-bullet--line">
                                    <span></span>
                                  </i>
                                  <span class="m-menu__link-text">
                                    Advance Trader
                                  </span>
                                </a>
                              </li>
                              <li class="m-menu__item "  data-redirect="true" aria-haspopup="true">
                                <a  href="reward_level" class="m-menu__link <?php if($page == 'reward_level' && $_COOKIE["level_title"] == 'Expert Trader' ){echo "menu__active"; } ?>" onclick="RewardLevel('Expert Trader','3')">
                                  <i class="m-menu__link-bullet m-menu__link-bullet--line">
                                    <span></span>
                                  </i>
                                  <span class="m-menu__link-text">
                                    Expert Trader
                                  </span>
                                </a>
                              </li>
                              <li class="m-menu__item "  data-redirect="true" aria-haspopup="true">
                                <a  href="reward_level" class="m-menu__link <?php if($page == 'reward_level' && $_COOKIE["level_title"] == 'Master Trader' ){echo "menu__active"; } ?>" onclick="RewardLevel('Master Trader','4')">
                                  <i class="m-menu__link-bullet m-menu__link-bullet--line">
                                    <span></span>
                                  </i>
                                  <span class="m-menu__link-text">
                                    Master Trader
                                  </span>
                                </a>
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </li>
                  <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel m-menu__item--submenu-tabs"  data-menu-submenu-toggle="click" data-redirect="true" aria-haspopup="true">
                    <a  href="#" class="m-menu__link m-menu__toggle <?php if($page == 'reward_item'){echo "menu__active"; } ?>">
                      <i class="m-menu__link-icon la la-gift"></i>
                      <span class="m-menu__link-text">
                        ข้อมูลของรางวัล
                      </span>
                      <i class="m-menu__hor-arrow la la-angle-down"></i>
                      <i class="m-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="m-menu__submenu  m-menu__submenu--fixed m-menu__submenu--left" style="width:300px">
                      <span class="m-menu__arrow m-menu__arrow--adjust"></span>
                      <div class="m-menu__subnav">
                        <ul class="m-menu__content">
                          <li class="m-menu__item">
                            <h3 class="m-menu__heading m-menu__toggle">
                              <span class="m-menu__link-text">
                                สถานะ
                              </span>
                              <i class="m-menu__ver-arrow la la-angle-right"></i>
                            </h3>
                            <ul class="m-menu__inner">
                              <li class="m-menu__item "  data-redirect="true" aria-haspopup="true">
                                <a  href="reward_items" class="m-menu__link <?php if($page == 'reward_item' && $_COOKIE["level_title"] == 'Go Trader' ){echo "menu__active"; } ?>" onclick="RewardLevel('Go Trader','1')">
                                  <i class="m-menu__link-bullet m-menu__link-bullet--line">
                                    <span></span>
                                  </i>
                                  <span class="m-menu__link-text">
                                    Go Trader
                                  </span>
                                </a>
                              </li>
                              <li class="m-menu__item "  data-redirect="true" aria-haspopup="true">
                                <a  href="reward_items" class="m-menu__link <?php if($page == 'reward_item' && $_COOKIE["level_title"] == 'Advance Trader' ){echo "menu__active"; } ?>" onclick="RewardLevel('Advance Trader','2')">
                                  <i class="m-menu__link-bullet m-menu__link-bullet--line">
                                    <span></span>
                                  </i>
                                  <span class="m-menu__link-text">
                                    Advance Trader
                                  </span>
                                </a>
                              </li>
                              <li class="m-menu__item "  data-redirect="true" aria-haspopup="true">
                                <a  href="reward_items" class="m-menu__link <?php if($page == 'reward_item' && $_COOKIE["level_title"] == 'Expert Trader' ){echo "menu__active"; } ?>" onclick="RewardLevel('Expert Trader','3')">
                                  <i class="m-menu__link-bullet m-menu__link-bullet--line">
                                    <span></span>
                                  </i>
                                  <span class="m-menu__link-text">
                                    Expert Trader
                                  </span>
                                </a>
                              </li>
                              <li class="m-menu__item "  data-redirect="true" aria-haspopup="true">
                                <a  href="reward_items" class="m-menu__link <?php if($page == 'reward_item' && $_COOKIE["level_title"] == 'Master Trader' ){echo "menu__active"; } ?>" onclick="RewardLevel('Master Trader','4')">
                                  <i class="m-menu__link-bullet m-menu__link-bullet--line">
                                    <span></span>
                                  </i>
                                  <span class="m-menu__link-text">
                                    Master Trader
                                  </span>
                                </a>
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </li>
                  <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel m-menu__item--submenu-tabs"  data-menu-submenu-toggle="click" data-redirect="true" aria-haspopup="true">
                    <a  href="#" class="m-menu__link m-menu__toggle <?php if($page == 'items_detail'){echo "menu__active"; } ?>">
                      <i class="m-menu__link-icon la la-file-text-o"></i>
                      <span class="m-menu__link-text">
                        รายละเอียดของรางวัล
                      </span>
                      <i class="m-menu__hor-arrow la la-angle-down"></i>
                      <i class="m-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="m-menu__submenu  m-menu__submenu--fixed m-menu__submenu--left" style="width:600px">
                      <span class="m-menu__arrow m-menu__arrow--adjust"></span>
                      <div class="m-menu__subnav">
                        <ul class="m-menu__content">
                          <li class="m-menu__item">
                            <h3 class="m-menu__heading m-menu__toggle">
                              <span class="m-menu__link-text">
                                ภาษา
                              </span>
                              <i class="m-menu__ver-arrow la la-angle-right"></i>
                            </h3>
                            <ul class="m-menu__inner">
                              <li class="m-menu__item "  data-redirect="true" aria-haspopup="true">
                                <a  href="items_detail" class="m-menu__link <?php if($page == 'items_detail'  && $_COOKIE["get_lang"] == 'th'){echo "menu__active"; } ?>" onclick="GetLang('th')">
                                  <i class="m-menu__link-icon flaticon-map"></i>
                                  <span class="m-menu__link-text">
                                    ไทย
                                  </span>
                                </a>
                              </li>
                              <li class="m-menu__item "  data-redirect="true" aria-haspopup="true">
                                <a  href="items_detail" class="m-menu__link <?php if($page == 'items_detail'  && $_COOKIE["get_lang"] == 'en'){echo "menu__active"; } ?>" onclick="GetLang('en')">
                                  <i class="m-menu__link-icon flaticon-user"></i>
                                  <span class="m-menu__link-text">
                                    อังกฤษ
                                  </span>
                                </a>
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </li>
          </ul>
        </div>
      </div>
      <!-- end::Horizontal Menu -->
    </div>
  </div>
</div>
</header>
<!-- end::Header -->
<script>
function RewardLevel(level_title,level_id){
  document.cookie = "level_title="+ level_title;
  document.cookie = "level_id="+ level_id;
  window.location = 'reward_level';
}
function GetLang(lang){
  document.cookie = "get_lang="+ lang;
  window.location = 'items_detail';
}
</script>
