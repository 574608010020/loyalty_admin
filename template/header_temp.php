<?php include("func/function.php"); ?>
<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			Loyalty | Dashboard
		</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--begin::Base Styles -->
        <!--begin::Page Vendors -->
		<link href="../assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Page Vendors -->
		<link href="../assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
		<link href="../assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Base Styles -->
		<link rel="shortcut icon" href="images/logo-mini.png" />
	</head>
	<!-- end::Head -->
    <!-- end::Body -->
	<body class=" <?php if($page == 'index'){echo 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default';}
											else{echo 'm-page--fluid m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default';}
								?>">
		<!-- begin::Page loader -->
		<div class="m-page-loader m-page-loader--base">
			<div class="m-blockui">
				<span>
					Please wait...
				</span>
				<span>
					<div class="m-loader m-loader--brand"></div>
				</span>
			</div>
		</div>
		<!-- end::Page Loader -->
    	<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<header class="m-grid__item		m-header "  data-minimize="minimize" data-minimize-mobile="minimize" data-minimize-offset="10" data-minimize-mobile-offset="10" >
				<div class="m-header__top">
					<div class="m-container m-container--fluid m-container--full-height m-page__container">
						<div class="m-stack m-stack--ver m-stack--desktop">
							<!-- begin::Brand -->
							<div class="m-stack__item m-brand m-stack__item--left">
								<div class="m-stack m-stack--ver m-stack--general m-stack--inline">
									<div class="m-stack__item m-stack__item--middle m-brand__logo">
										<a href="index.html" class="m-brand__logo-wrapper">
											<img alt="" src="images/logo.png" class="m-brand__logo-desktop"/>
			                <img alt="" src="images/logo.png" class="m-brand__logo-mobile"/>
										</a>
									</div>
									<div class="m-stack__item m-stack__item--middle m-brand__tools">
						<!-- begin::Responsive Header Menu Toggler-->
										<a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
											<span></span>
										</a>
										<!-- end::Responsive Header Menu Toggler-->
			<!-- begin::Topbar Toggler-->
										<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
											<i class="flaticon-more"></i>
										</a>
										<!--end::Topbar Toggler-->
									</div>
								</div>
							</div>
							<!-- end::Brand -->
							<!-- begin::Topbar -->
						        <div class="m-stack__item m-stack__item--right m-header-head" id="m_header_nav">
						          <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
						            <div class="m-stack__item m-topbar__nav-wrapper">
						              <ul class="m-topbar__nav m-nav m-nav--inline">
						                <li class="m-nav__item m-dropdown m-dropdown--medium m-dropdown--arrow  m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" data-dropdown-toggle="click">
						                  <a href="#" class="m-nav__link m-dropdown__toggle">
						                    <span class="m-topbar__userpic">
						                      <img src="assets/app/media/img/users/user4.jpg" class="m--img-rounded m--marginless m--img-centered" alt=""/>
																	<h2>Mark Andre</h2>
						                    </span>
						                    <span class="m-nav__link-icon m-topbar__usericon  m--hide">
						                      <span class="m-nav__link-icon-wrapper">
						                        <i class="flaticon-user-ok"></i>
						                      </span>
						                    </span>
						                    <span class="m-topbar__username m--hide">
						                      Mark
						                    </span>
						                  </a>
						                  <div class="m-dropdown__wrapper">
						                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
						                    <div class="m-dropdown__inner">
						                      <div class="m-dropdown__header m--align-center">
						                        <div class="m-card-user m-card-user--skin-light">
						                          <div class="m-card-user__pic">
						                            <img src="assets/app/media/img/users/user4.jpg" class="m--img-rounded m--marginless" alt=""/>
						                          </div>
						                          <div class="m-card-user__details">
						                            <span class="m-card-user__name m--font-weight-500">
						                              Mark Andre
						                            </span>
						                            <a href="" class="m-card-user__email m--font-weight-300 m-link">
						                              mark.andre@gmail.com
						                            </a>
						                            <hr>
						                            <a href="snippets/pages/user/login-1.html" class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
						                              Logout
						                            </a>
						                          </div>
						                        </div>
						                      </div>
						                    </div>
						                  </div>
						                </li>
						              </ul>
						            </div>
						          </div>
						        </div>
						        <!-- end::Topbar -->
						      </div>
						    </div>
						  </div>
